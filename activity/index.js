function oddEvenChecker(number) {

	if(typeof number === "number"){
	
		if(number % 2 === 0){
			console.log("The number is even.")
		} else {
			console.log("The number is odd.")
		}

	} else {
		console.log("Invalid input")
	}

}


function budgetChecker(budget) {

	if(typeof budget === "number") {

		if(budget > 40000){
			console.log("You are over the budget.")
		} else {
			console.log("You have resources left.")
		}

	} else {
		console.log("Invalid Input")
	}
}